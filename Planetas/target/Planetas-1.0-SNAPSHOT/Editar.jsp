<%-- 
    Document   : Editar
    Created on : 8/06/2020, 05:59:01 PM
    Author     : PROBOOK 6470B
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
        import ="java.sql.Connection"        
        import ="java.sql.DriverManager"        
        import ="java.sql.ResultSet"        
        import ="java.sql.Statement"        
        import ="java.sql.SQLException"  
        %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar</title>
        <link rel="stylesheet"href="Editar.css">
    </head>
    <body background="blackHole.jpg">
         <h1><strong><font color="white">Cosmos Gromy</font></strong></h1>
<table border="1">
            <p><strong>Mis planetas</strong></p>

            <tr>
                <th>ID del Planeta</th>
                <th>Nombre del Planeta</th>
                <th>Informacion del planeta</th>
                <th>Fecha de creación</th>
                
            </tr>


            <%
                Connection conex = null;
                Statement sql = null;
                try {
                    Class.forName("com.mysql.cj.jdbc.Driver");
                     conex = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/PlanetasWeb?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "2203");
                    sql = conex.createStatement();

                    String qry = "SELECT * FROM PlanetasWeb.registros";
                    ResultSet data = sql.executeQuery(qry);
                    while (data.next()) {
            %>

            <% if(data.getInt("Visibilidad") == 1){ %>
            <tr>
                <td>
                    <% out.print(data.getInt("Id"));%>
                </td>
                <td>
                      <% if(data.getInt("Planeta_Id")==1){%>
                            Mercurio
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==2){%>
                            Venus
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==3){%>
                            Tierra
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==4){%>
                            Marte
                            <% } %>
                        
                    <% if(data.getInt("Planeta_Id")==5){%>
                            Jupiter
                            <% } %>
                            
                    <% if(data.getInt("Planeta_Id")==6){%>
                            Saturno
                            <% } %>
                            
                    <% if(data.getInt("Planeta_Id")==7){%>
                            Urano
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==8){%>
                            Neptuno
                            <% } %>
                </td>
                <td>
                    <% out.print(data.getString("Informacion del Planeta"));%>
                </td>
                <td>
                    <% out.print(data.getString("Fecha de creacion"));%>
                </td>


            </tr>

            <%  } %>

            <%
                    }
                    data.close();

                } catch (Exception e) {
                    out.print("Error en la conexión. Intenta de nuevo!");
                    e.printStackTrace();
                }

            %>
        </table>
        
        <p></p>
        <form name="Registro" action="Sobreescritura.jsp" method="get">
            
            
            <p>¿Cual es la posicion que deseas editar? <input type="text" name="ID" value=""/> </p><br>
                
                
            <p>Selecciona un Planeta:</p>
            
            <div>

            <input type="radio" id="1" name="Planeta" value="1">
            <label for="male">Mercurio</label><br>
            <input type="radio" id="2" name="Planeta" value="2">
            <label for="female">Venus</label><br>
            <input type="radio" id="3" name="Planeta" value="3">
            <label for="other">Tierra</label><br>
            <input type="radio" id="4" name="Planeta" value="4">
            <label for="other">Marte</label><br>
            <input type="radio" id="5" name="Planeta" value="5">
            <label for="other">Jupiter</label><br>
            <input type="radio" id="6" name="Planeta" value="6">
            <label for="other">Saturno</label><br>
            <input type="radio" id="7" name="Planeta" value="7">
            <label for="other">Urano</label><br>
            <input type="radio" id="8" name="Planeta" value="8">
            <label for="other">Neptuno</label><br>
            
            </div>
            
            <p></p>
           
            <p> Introduce la informacion de tu Planeta:</p>
             
            <textarea name="Entrada" rows="10" cols="60"></textarea><br>
            
                
            <input type="submit" value="Editar Planeta">
            
            <p></p>
            
        </form>
        
        <p></p>
        <button onclick="window.location.href = 'Eliminar.jsp';">Eliminar un Planeta</button>
        <p></p>

        <button onclick="window.location.href = 'index.html';">Regresar al menú principal</button>

    </body>
</html>
