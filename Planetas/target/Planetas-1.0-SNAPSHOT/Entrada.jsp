<%-- 
    Document   : Entrada
    Created on : 8/06/2020, 03:29:40 PM
    Author     : PROBOOK 6470B
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Añade un planeta a tu diario</title>
        <link rel="stylesheet"href="Entrada.css">
    </head>
    <body background="Planetas2.jpg">
        <h1><strong><font color="white">Cosmos Gromy</font></strong></h1>
        
        <form name="Registro" action="data.jsp" method="get">
                
            <p><font color="white">Selecciona el nombre del Planeta:</font></p>

            <div>
            <input type="radio" id="1" name="Planeta" value="1">
            <label for="male">Mercurio</label><br>
            <input type="radio" id="2" name="Planeta" value="2">
            <label for="female">Venus</label><br>
            <input type="radio" id="3" name="Planeta" value="3">
            <label for="other">Tierra</label><br>
            <input type="radio" id="4" name="Planeta" value="4">
            <label for="other">Marte</label><br>
            <input type="radio" id="5" name="Planeta" value="5">
            <label for="other">Jupiter</label><br>
            <input type="radio" id="6" name="Planeta" value="6">
            <label for="other">Saturno</label><br>
            <input type="radio" id="7" name="Planeta" value="7">
            <label for="other">Urano</label><br>
            <input type="radio" id="8" name="Planeta" value="8">
            <label for="other">Neptuno</label><br>
            
            </div>
            
      
                <p><font color="white"> Introduce la informacion de tu Planeta:</font></p>
            
           
            <textarea name="Entrada" rows="10" cols="60"></textarea>
            
            <p></p>
            <input type="submit" value="Añadir Planeta">
        </form>
        <p></p>

        <button onclick="window.location.href = 'index.html';">Regresar al menú principal</button>
    </body>
</html>
