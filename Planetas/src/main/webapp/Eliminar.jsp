<%-- 
    Document   : Eliminar
    Created on : 9/06/2020, 12:47:56 AM
    Author     : PROBOOK 6470B
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import= "java.sql.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar</title>
        <link rel="stylesheet"href="Eliminar.css">
    </head>
    <body background="space.jpg">
         <h1><strong><font color="white">Cosmos Gromy</font></strong></h1>
   
         <p>Mis Planetas</p>
         
        <table border="1">
        <tr>
                <th>Posicion</th>
                <th>Nombre del planeta</th>
                <th>Informacion del planeta</th>
                <th>Fecha de creación</th>
        </tr>


            <%
                Connection conex = null;
                Statement sql = null;
                try {
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    conex = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/PlanetasWeb?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "2203");
                    sql = conex.createStatement();

                    String qry = "select * from PlanetasWeb.registros";
                    ResultSet data = sql.executeQuery(qry);
                    while (data.next()) {
            %>

              <% if(data.getInt("Visibilidad") == 1){ %>
            <tr>
                <td>
                    <% out.print(data.getInt("Id"));%>
                </td>
                <td>
                    <% if(data.getInt("Planeta_Id")==1){%>
                            Mercurio
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==2){%>
                            Venus
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==3){%>
                            Tierra
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==4){%>
                            Marte
                            <% } %>
                        
                    <% if(data.getInt("Planeta_Id")==5){%>
                            Jupiter
                            <% } %>
                            
                    <% if(data.getInt("Planeta_Id")==6){%>
                            Saturno
                            <% } %>
                            
                    <% if(data.getInt("Planeta_Id")==7){%>
                            Urano
                            <% } %>
                    
                    <% if(data.getInt("Planeta_Id")==8){%>
                            Neptuno
                            <% } %>
                 
               </td> 
                <td>
                   <% out.print(data.getString("Informacion del Planeta"));%> 

                </td>
                <td>
                    <% out.print(data.getString("Fecha de creacion"));%>
                </td>
                
            </tr>

             <%  } %>
            <%
                    }
                    data.close();

                } catch (Exception e) {
                    out.print("Error en la conexión. Intenta de nuevo!");
                    e.printStackTrace();
                }

            %>
        </table>
        
        <p></p>
        
        <form name="Registro" action="dataEliminar.jsp" method="get">
            
            <p>Ingresa la posicion del planeta que deseas eliminar: <input type="text" name="ID" value=""/> </p>
                
            <input type="submit" value="Eliminar">
            
            <p></p>
            
        </form>
        
        <p></p>
        <button onclick="window.location.href = 'index.html';">Regresar al menú principal</button>
        
    </body>
</html>
